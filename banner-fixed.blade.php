<div class="position-fixed banner-float-left">{!! $floatLeft !!}</div>
<div class="position-fixed banner-float-right">{!! $floatRight !!}</div>
<div class="position-fixed banner-balloon-left">{!! $balloonLeft !!}</div>
<div class="position-fixed banner-balloon-right">{!! $balloonRight !!}</div>
<div class="container position-fixed banner-catfish px-0 px-lg-3">{!! $catfish !!}</div>
@if(!empty($popup))
<div class="ads-modal position-fixed @if(IS_AMP) d-flex @else d-none @endif align-items-center justify-content-center" id="banner-popup">
    <div class="box-modal position-absolute" role="button" tabindex="-1" on="tap:banner-popup.hide"></div>
    {!! $popup !!}
</div>
@endif
