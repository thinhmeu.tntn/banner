<?php

namespace App\Helpers\banner;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\View;

class Banner
{
    public function handle($request, \Closure $next){
        if (!defined("IS_AMP"))
            define("IS_AMP", 0);
        if (!defined("IS_MOBILE"))
            define("IS_MOBILE", 0);

        $response = $next($request);
        if ($response instanceof Response){
            $content = $response->getContent();

            $css = $this->getCss();
            if (IS_AMP){
                $content = str_replace("<style amp-custom>", "<style amp-custom>$css", $content);
            } else {
                $content = preg_replace("#</head>#", "<style>$css</style></head>", $content, 1);
            }

            $content = $this->key2Banner($content);

            $bannerTop = $this->getBannerTop();
            $content = preg_replace("#</header>#", "</header>$bannerTop", $content, 1);

            $bannerFixed = $this->getBannerFixed();
            $content = preg_replace("#</body>#", "$bannerFixed</body>", $content, 1);

            if (!IS_AMP){
                $js = $this->getJs();
                $content = preg_replace("#</body>#", "<script>$js</script></body>", $content, 1);
            }

            $response->setContent($content);
        }
        return $response;
    }
    private function getCss(){
        return file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'banner.css');
    }
    private function getJs(){
        $newStatis = (new static());
        $dataPopunder = $newStatis::mobile("popunder-mobile") ?? $newStatis::pc("popunder-pc");

        $contentJs = file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'banner.js');
        return '
        var dataPopunder ='.json_encode($dataPopunder).';'.$contentJs;
    }
    private function key2Banner($content){
        $arrValue = [];
        preg_match_all("#\[banner-(.*?)\]#", $content, $position);
        foreach ($position[1] as $key){
            preg_match("#-\w*?$#", $key, $check);
            switch ($check[0] ?? ''){
                case "-pc":
                    $arrValue[] = self::pc($key); break;
                case "-mobile":
                    $arrValue[] = self::mobile($key); break;
                default:
                    $arrValue[] = self::renderBanner($key);
            }
        }
        return str_replace($position[0], $arrValue, $content);
    }
    private function getBannerFixed(){
        $floatLeft = self::pc('float-left-pc') ?? self::mobile("float-left-mobile");
        $floatRight = self::pc('float-right-pc') ?? self::mobile("float-right-mobile");
        $balloonLeft = self::pc('balloon-left-pc');
        $balloonRight = self::pc('balloon-right-pc');
        $catfish = self::pc("catfish-pc", "banner-column-2") ?? self::mobile("catfish-mobile");
        if (substr_count($catfish, '<a') == 1)
            $catfish = str_replace('banner-column-2', '', $catfish);
        $popup = self::pc("popup-pc") ?? self::mobile("popup-mobile");

        $data = [
            'floatLeft' => $floatLeft,
            'floatRight' => $floatRight,
            'balloonLeft' => $balloonLeft,
            'balloonRight' => $balloonRight,
            'catfish' => $catfish,
            'popup' => $popup
        ];
        return View::file(__DIR__.DIRECTORY_SEPARATOR."banner-fixed.blade.php", $data)->render();
    }
    private function getBannerTop(){
        $bannerTop = self::pc('full-width-pc', 'banner-column-2') ?? self::mobile("header-mobile");

        if (substr_count($bannerTop, '<a') == 1)
            $bannerTop = str_replace('banner-column-2', '', $bannerTop);

        $data = [
            'bannerTop' => $bannerTop
        ];
        return View::file(__DIR__.DIRECTORY_SEPARATOR."banner-top.blade.php", $data)->render();
    }

    public static function pc($slug, $customClass = ''){
        if (IS_MOBILE)
            return null;
        return self::renderBanner($slug, $customClass);
    }
    public static function mobile($slug, $customClass = ''){
        if (!IS_MOBILE)
            return null;
        return self::renderBanner($slug, $customClass);
    }
    private static function renderBanner($slug, $customClass = ''){
        if (!isset(self::getBannerFromApi()[$slug]))
            return '';
        $banner =  self::getBannerFromApi()[$slug];

        if (in_array($slug, ['popunder-mobile', 'popunder-pc'])){
            return [
                'link' => $banner[0]['link'],
                'max' => $banner[0]['nums_of_show']
            ];
        }

        $data = ['slug' => $slug, 'data' => $banner, 'customClass' => $customClass];

        return View::file(__DIR__.DIRECTORY_SEPARATOR.'_items.blade.php', $data)->render();
    }
    private static function getBannerFromApi():array{
        $keyCache = 'listBanner';
        $banners = Cache::get($keyCache);
        if (empty($banners)) {
            $bannerApi = Http::get('https://omegaads.live/api/banner/'.env("ID_BANNER", "demo"));
            if ($bannerApi->status() == 200) {
                $banners = $bannerApi->json();
            }
            Cache::put($keyCache, $banners, 60);
        }
        return $banners ?? [];
    }
}
