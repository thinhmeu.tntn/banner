<div class="box-ads position-relative {{$customClass}}" id="{{$slug}}" data-max="{{$data[0]['nums_of_show']}}">
    <div class="position-absolute box-button">
        <b>ⓘ</b>
        <b class="ads-close" role="button" tabindex="-1" on="tap:{{$slug}}.hide,banner-popup.hide">✕</b>
    </div>
    @foreach($data as $i)
        @if($i['type'] == 'default')
            <a class="linkBanner" href="{{$i['link']}}" target="{{$i['target']}}" rel="{{$i['rel']}}" title="{{$i['alt']}}">
                <img loading="lazy" width="{{$i['width']}}" height="{{$i['height']}}" src="{{$i['image']}}" alt="{{$i['alt']}}">
            </a>
        @else
            {!! $i['content'] !!}
        @endif
    @endforeach
</div>
