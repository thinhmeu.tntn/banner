const Banner = {
    keyPopup: "banner-popup",
    keyPopunder: "banner-popunder",
    checkPopup(id){
        return ["popup-pc", 'popup-mobile'].includes(id);
    },
    isOverLimit (id, max){
        if (typeof max === "undefined")
            try {
                max = parseInt(document.getElementById(id).getAttribute("data-max"));
            } catch (error) {
                max = 0;
            }
        else
            max = parseInt(max);
        let showed = parseInt(sessionStorage.getItem(id));
        return showed >= max && max > 0;
    },
    removeIfMax(){
        let arrPosition = Object.keys(sessionStorage);
        let keyBanners = arrPosition.filter((i) => {
            return i !== Banner.keyPopunder && i !== Banner.keyPopup;
        })
        keyBanners.forEach((i) => {
            let banner = document.getElementById(i);
            if (Banner.isOverLimit(i))
                banner.remove();
        })
    },
    removeIfMaxPopunder(){
        let keyPopunder = Banner.keyPopunder;
        if (Banner.isOverLimit(keyPopunder, dataPopunder.max ?? 0))
            dataPopunder = {};
    },
    removeIfMaxPopup(){
        let keyPopup = Banner.keyPopup;
        let boxPopup = document.getElementById(Banner.keyPopup);
        if(!boxPopup)
            return;
        let bannerPopup = boxPopup.querySelector('[data-max]');
        let max = bannerPopup.getAttribute("data-max");
        if (Banner.isOverLimit(keyPopup, max))
            boxPopup.remove();
    },
    hidePopup(){
        document.getElementById(Banner.keyPopup).remove();
        Banner.addBannerToSession(Banner.keyPopup);
    },
    showPopup(){
        let selectorPopup = document.getElementById(Banner.keyPopup);
        if (!selectorPopup)
            return;
        selectorPopup.classList.replace("d-none", "d-flex");
    },
    onPopunder(){
        let keyPopunder = Banner.keyPopunder;
        document.querySelectorAll("a:not(.linkBanner):not([target=_blank])").forEach((i)=>{
            i.addEventListener("click", function(e) {
                if (dataPopunder.link){
                    e.preventDefault();
                    let currentUrl = this.getAttribute("href");

                    Banner.addBannerToSession(keyPopunder);
                    window.open(currentUrl, "_blank");
                    window.location = dataPopunder.link;
                }
            });
        });
    },
    onBanner(){
        document.querySelectorAll(".box-ads .ads-close").forEach((i)=>{
            i.addEventListener("click", function () {
                let banner = this.closest(".box-ads");
                let id = banner.getAttribute("id");
                if (Banner.checkPopup(id)){
                    Banner.hidePopup();
                } else {
                    banner.remove();
                    Banner.addBannerToSession(id);
                }
            })
        })
    },
    addBannerToSession(id){
        let showed = parseInt(sessionStorage.getItem(id) ?? 0);
        sessionStorage.setItem(id, showed + 1);
    },
    init(){
        this.removeIfMax();
        this.removeIfMaxPopup();
        this.removeIfMaxPopunder();
        this.onBanner();
        this.onPopunder();
        this.showPopup();
        let popup = document.querySelector(`[on="tap:${Banner.keyPopup}.hide"]`);
        if (popup)
            popup.addEventListener("click", ()=>{
                Banner.hidePopup();
            });
    }
}
document.addEventListener("DOMContentLoaded", function() {
    if (dataPopunder === '')
        dataPopunder = {};
    Banner.init();
});
